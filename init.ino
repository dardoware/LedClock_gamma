// Custom chars :)
byte icono_luz_auto[8] = {
  B10010,
  B00100,
  B10000,
  B01011,
  B10000,
  B00100,
  B10010,
};

byte icono_luz_manual[8] = {
  B00100,
  B01110,
  B11111,
  B00000,
  B10101,
  B10101,
  B10101,
};

byte icono_centigrados[8]{
  B01000,
  B10100,
  B01000,
  B00111,
  B00100,
  B00100,
  B00111,
};

void inicializacion() {
  Serial.begin(VELOCIDAD_SERIAL);
  Serial.print("conetcado a ");
  Serial.print(VELOCIDAD_SERIAL);
  Serial.println(" baudios");
  
  lcd.begin (LCD_COL,LCD_FILAS);
  lcd.setBacklightPin(BACKLIGHT_PIN,POSITIVE);
  lcd.setBacklight(HIGH);
  lcd.createChar(0, icono_luz_auto);
  lcd.createChar(1, icono_luz_manual);
  lcd.createChar(2, icono_centigrados);
  
  irrecv.enableIRIn();
  FastLED.addLeds<LEDS_CHIPSET, LEDS_DATA, LEDS_ORDEN_COLORES>(corona, TOTAL_LEDS).setCorrection(Typical8mmPixel);
  
  establecePaletaParchis();

  temp = ((analogRead(SENSOR_TEMP)*0.004882814)-0.5)*100;
  
}


