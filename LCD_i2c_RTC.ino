#define FASTLED_ALLOW_INTERRUPTS 0
#include "config.h"
#include <Wire.h>
#include <LCD.h>
#include <LiquidCrystal_I2C.h>
#include "RTClib.h"
#include <IRremote.h>
#include "FastLED.h"

RTC_DS1307 rtc;
unsigned long preTimerLCD = 0;
unsigned long preTimerSensores=0;
unsigned long preTimerLuzLCD=0;
unsigned long preTimerFastLED=0;

LiquidCrystal_I2C	lcd(I2C_ADDR,En_pin,Rw_pin,Rs_pin,D4_pin,D5_pin,D6_pin,D7_pin);

IRrecv irrecv(RECEP_MANDO);
decode_results mando;
int ultimo_comando =0;

byte modo_funcionamiento = MODO_DEFAULT;

boolean brillo_automatico = true;
byte brillo = BRILLO_INICIAL;
byte brillo_manual = BRILLO_INICIAL;
byte icono_luz = 0;
int temp = 10;

byte paleta = PALETA_DEFAULT;
CRGBPalette16 paletaEnUso;
TBlendType mezcla;
CRGB corona[TOTAL_LEDS];


void setup()
{
  inicializacion();
}

void loop()
{
  DateTime ahora = rtc.now();
  
  fijarBrillo();
  
  unsigned long ahoraMillis = millis();
  if(ahoraMillis - preTimerSensores >= SENSORES_REFRESH) { 
    preTimerSensores = ahoraMillis;   
    temp = ((analogRead(SENSOR_TEMP)*0.004882814)-0.5)*100;
  }
   
  ahoraMillis = millis();
  if(ahoraMillis - preTimerLCD >= DISPLAY_REFRESH) { 
    preTimerLCD = ahoraMillis;   
    pintarDisplay(ahora);
  }
  

 
  ahoraMillis = millis();
  if(ahoraMillis - preTimerLuzLCD >= DISPLAY_TIMEOUT) {   
      lcd.setBacklight(LOW);
  }
  
  leerMando();
  
  ahoraMillis = millis();
  if(ahoraMillis - preTimerFastLED >= 1000/8) {
    preTimerFastLED = ahoraMillis;
    switch (modo_funcionamiento) {
      case 0: pintarRelojSimple(ahora); break;
      case 1: pintarRelojArcos(ahora); break;
      case 2: hacerAlgo(); break;
      case 3: hacerAlgo(); break;
//      default: modo_funcionamiento = 0; break;
    }

    FastLED.show(brillo);
  }
}

void hacerAlgo() {
  delay(0);
  }

  
void pintarDisplay(DateTime h) {
  lcd.home ();
  lcd.clear();
  
  if (h.day() <10) {
    lcd.print("0");
  } 
  lcd.print(h.day());
  lcd.print("/"); 
 
  if (h.month() <10) {
    lcd.print("0");
  }
 
  lcd.print(h.month());
  lcd.print("/"); 
  lcd.print(h.year());
 
  lcd.print(" ");
  if (h.hour() <10) {
    lcd.print("0");
  } 
  lcd.print(h.hour());
 
  lcd.print(":");
  if (h.minute() <10) {
    lcd.print("0");
  }   
  lcd.print(h.minute());
 
  lcd.setCursor (0,1);
  lcd.print(char(icono_luz));
  lcd.print(brillo);
 
  lcd.setCursor(5,1);
  lcd.print(temp);
  lcd.print(char(2));
 
}

void leerMando() {
  
  if (irrecv.decode(&mando)) {
    switch (mando.value) {
      case COMANDO_LETARGO:          Serial.println("Letargo"); funcionLetargo(); break;
      case COMANDO_REPETIR:          repetirComando(); break;
      case COMANDO_DESPERTAR:        Serial.println("Despertar"); funcionDespertar(); break;
      case COMANDO_MODO_BRILLO:      cambiarBrillo(); funcionDespertar(); break;
      case COMANDO_BAJAR_BRILLO:     bajarBrillo(); funcionDespertar(); break;
      case COMANDO_SUBIR_BRILLO:     subirBrillo(); funcionDespertar(); break;
      case COMANDO_PALETA_SIGUIENTE: funcionSubirPaleta(); funcionDespertar(); break;
      case COMANDO_PROXIMO_ESQUEMA:  funcionFijarModoVisualizacion(); funcionDespertar(); break;
      default: Serial.print(mando.value, HEX); Serial.println(": Funcion no programada"); break;
    }
    irrecv.resume(); 
  }
}

void funcionSubirPaleta() {
  if (paleta<PALETAS_TOTALES-1) {
    paleta++;
  }
  else {
    paleta=0;
  }
  switch (paleta) {
    case 0: establecePaletaParchis();break;
    case 1: establecePaletaFuego(); break;
    case 2: establecePaletaCielo(); break;
    case 3: establecePaletaBosque(); break;
    default: paleta=0; establecePaletaParchis(); break;
  }
  Serial.print("Paleta ");
  Serial.println(paleta);
  
  
}

void repetirComando() {
  switch (ultimo_comando) {
    case COMANDO_BAJAR_BRILLO: bajarBrillo(); funcionDespertar(); break;
    case COMANDO_SUBIR_BRILLO: subirBrillo(); funcionDespertar(); break;
    default: ultimo_comando=COMANDO_REPETIR; break;    
  }
}

void bajarBrillo() {
  if (!brillo_automatico && brillo_manual > BRILLO_MIN) { 
    brillo_manual--;
    Serial.print("Brillo: ");
    Serial.println(brillo_manual);
    ultimo_comando=COMANDO_BAJAR_BRILLO;
  }
}

void subirBrillo() {
  if (!brillo_automatico&& brillo_manual < BRILLO_MAX) { 
    brillo_manual++;
    Serial.print("Brillo: ");
    Serial.println(brillo_manual);
    ultimo_comando=COMANDO_SUBIR_BRILLO;
  }
}

void cambiarBrillo(){
  brillo_automatico=!brillo_automatico;
    if (brillo_automatico) {
      icono_luz=0;
      Serial.println("Brillo automatico");
    }
    else {
      icono_luz=1;
      Serial.println("Brillo manual");
    }
}

void funcionLetargo() {
    lcd.setBacklight(LOW);
    ultimo_comando=COMANDO_LETARGO;
}

void funcionDespertar() {
    lcd.setBacklight(HIGH);
    preTimerLuzLCD=millis();
}

void fijarBrillo() {
  if (brillo_automatico) {
    brillo=map(analogRead(SENSOR_LUZ),0,1023,BRILLO_MIN,BRILLO_MAX);
  }
  else {
    brillo=brillo_manual;
  }
}

void funcionFijarModoVisualizacion() {
  if (modo_funcionamiento < MODOS_TOTALES-1) {
    modo_funcionamiento++;
  } else {
    modo_funcionamiento = 0;
  }
  Serial.print("Modo de visualizacion: "); 
  Serial.println(modo_funcionamiento);
}
