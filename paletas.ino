void establecePaletaParchis()
{
  CRGB rojo      = CRGB::Red;
  CRGB verde     = CRGB::Green;
  CRGB amarillo  = CRGB::Yellow;
  CRGB azul      = CRGB::Blue;
  CRGB negro     = CRGB::Black;
  
  paletaEnUso = CRGBPalette16( 
    rojo,  verde,  amarillo,  azul,
    negro, negro, negro, negro, 
    negro, negro, negro, negro,
    negro, negro, negro, negro );
}

void establecePaletaFuego()
{
  CRGB rojo =     CHSV( HUE_YELLOW, 255, 255);
  CRGB naranja =  CHSV(HUE_ORANGE,255,255);
  CRGB amarillo = CHSV( HUE_YELLOW, 255, 255);
  CRGB rojoOscuro  = CRGB::DarkRed;
  CRGB negro    = CRGB::Black;
  
  paletaEnUso = CRGBPalette16( 
    rojo,  naranja,  amarillo,  rojoOscuro,
    negro, negro, negro, negro, 
    negro, negro, negro, negro,
    negro, negro, negro, negro );
}

void establecePaletaCielo()
{
  CRGB azul       = CRGB::Blue;
  CRGB celeste    = CRGB::SkyBlue;
  CRGB blanco     = CRGB::Gray;
  CRGB azulOscuro = CRGB::DarkBlue;
  CRGB negro      = CRGB::Black;
  
  paletaEnUso = CRGBPalette16( 
    azul,  celeste,  blanco, azulOscuro,
    negro, negro, negro, negro, 
    negro, negro, negro, negro,
    negro, negro, negro, negro );
}

void establecePaletaBosque()
{
  CRGB verde      = CRGB::Green;
  CRGB marron     = CRGB::SaddleBrown ;
  CRGB lima       = CRGB::Lime;
  CRGB verdeOscuro = CRGB::DarkGreen;
  CRGB negro       = CRGB::Black;
  
  paletaEnUso = CRGBPalette16( 
    verde,  verdeOscuro,  lima,  marron,
    negro, negro, negro, negro, 
    negro, negro, negro, negro,
    negro, negro, negro, negro );
}

