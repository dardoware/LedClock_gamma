void pintarRelojSimple(DateTime h) {
  fill_solid( &(corona[0]), TOTAL_LEDS,CRGB(0,0,0));

  int led_hh=(venticuatroDoce(h.hour()))*5;
  corona[es60(led_hh)] = ColorFromPalette( paletaEnUso,0*16, brillo, NOBLEND);
  corona[es60(led_hh-1)] = ColorFromPalette( paletaEnUso,0*16, brillo, NOBLEND);
  corona[es60(led_hh+1)] = ColorFromPalette( paletaEnUso,0*16, brillo, NOBLEND);

  corona[h.minute()] = ColorFromPalette( paletaEnUso,1*16, brillo, NOBLEND);
  
  corona[h.second()] = ColorFromPalette( paletaEnUso,2*16, brillo, NOBLEND);

}

void setPuntosHora(DateTime h) {
  for (int i=0; i< TOTAL_LEDS; i+5) {
    corona[i]=ColorFromPalette(paletaEnUso,3,brillo/2,NOBLEND);
  } 
}

void pintarRelojArcos(DateTime h) {
  int i=0;
  fill_solid( &(corona[0]), TOTAL_LEDS, CRGB(0,0,0) );

  int led_hh=(venticuatroDoce(h.hour()))*5;
  for (i=0; i<led_hh; i++) {
    corona[i] = ColorFromPalette( paletaEnUso,0*16, brillo, NOBLEND);
  }
  
  for (i=0; i<h.minute(); i++) {
    corona[i] += ColorFromPalette( paletaEnUso,1*16, brillo, NOBLEND);
  }
}



int es60(int n) {
  n=n%60;
  return n;
}

int venticuatroDoce(int n) {
  n=n%12;
  return n;
}
