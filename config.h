// Configuración general
#define VELOCIDAD_SERIAL 115200

// COnfiguración del display LC2
#define LCD_COL 16
#define LCD_FILAS 2
#define I2C_ADDR    0x27
#define BACKLIGHT_PIN     3
#define En_pin  2
#define Rw_pin  1
#define Rs_pin  0
#define D4_pin  4
#define D5_pin  5
#define D6_pin  6
#define D7_pin  7

#define DISPLAY_REFRESH 1000
#define DISPLAY_TIMEOUT 10000

#define SENSORES_REFRESH 5000


// Parametros del brillo
#define BRILLO_INICIAL 128
#define BRILLO_MIN 30
#define BRILLO_MAX 255



//  Configuración del mando a distancia

#define COMANDO_LETARGO          0xFFE21D          //OFF
#define COMANDO_REPETIR          0xFFFFFFFF        //Cuando se deja una tecla pulsada
#define COMANDO_DESPERTAR        0xFFA25D          //ON
#define COMANDO_SUBIR_BRILLO     0xFF9867          //FLECHA ARRIBA
#define COMANDO_BAJAR_BRILLO     0xFFB04F          //FLECHA ABAJO
#define COMANDO_MODO_BRILLO      0xFF6897          //TECLA BLANCA
#define COMANDO_PALETA_SIGUIENTE 0xFFA857          //TECLA MORADA
#define COMANDO_PROXIMO_ESQUEMA  0xFF906F          //TECLA AZUL CLARO


#define MODO_DEFAULT    0
#define MODOS_TOTALES   2
#define PALETA_DEFAULT  0
#define PALETAS_TOTALES 4

// Leds
#define LEDS_CHIPSET       WS2811
#define LEDS_ORDEN_COLORES RGB
#define TOTAL_LEDS         60


// Pines Arduino

#define SENSOR_LUZ  A0
#define SENSOR_TEMP A1

#define LEDS_DATA   10
#define RECEP_MANDO 11
